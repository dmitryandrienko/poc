// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BundleConfig.cs" company="">
//   Copyright � 2015 
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace App.ChartsPoC
{
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/styles/css").Include(
                "~/content/app.css",
                "~/content/themes/base/all.css"));

            bundles.Add(new ScriptBundle("~/js/jquery").Include("~/scripts/vendor/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/js/charts").Include(
                //"~/scripts/highcharts/highstock.js",
                "~/scripts/highcharts/highcharts.js",
                "~/scripts/highcharts/highcharts-3d.js",
                "~/scripts/angular-highcharts.js"));

            bundles.Add(new ScriptBundle("~/js/slider").Include(
                "~/scripts/jquery-ui-1.11.2.js",
                "~/scripts/slider.js"));

            bundles.Add(new ScriptBundle("~/js/app").Include(
                "~/scripts/ui-bootstrap-tpls-0.12.1.min.js",
                "~/scripts/vendor/angular-ui-router.js",
                "~/scripts/filters.js",
                "~/scripts/services.js",
                "~/scripts/directives.js",
                "~/scripts/controllers.js",
                "~/scripts/app.js"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
