﻿'use strict';

angular.module('app.filters', [])
    .filter('interpolate', ['version', function(version) {
        return function(text) {
            return String(text).replace(/\%VERSION\%/mg, version);
        };
    }])
    .filter('yes_no', function() {
        return function(text, length, end) {
            if (text) {
                return 'Yes';
            }
            return 'No';
        };
    })
    .filter('percentage', ['$filter', function($filter) {
        return function(input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }])
    .filter('shortenCurrency2', ['$filter', function ($filter) {
        return function (input, curSymbol, decimal) {
            var rv = curSymbol;
            if (input >= 1000000) {
                rv += $filter('number')(input / 1000000, decimal) + 'M';
            }
            else if (input >= 1000) {
                rv += $filter('number')(input / 1000, decimal) + 'K';
            } else {
                rv += input;
            }
            return rv;
        };
    }])
    .filter('shortenCurrency', ['$filter', function ($filter) {
        return function (input, curSymbol, thousand, decimal) {
            var rv = curSymbol;
            if (input >= 1000) {
                rv += $filter('number')(input / 1000, decimal) + thousand;
            } else {
                rv += input;
            }
            return rv;
        };
    }]);