﻿'use strict';

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('app.services', [])

    .value('version', '0.1')
    
    .factory('math' , [function() {
        
        function IRR(values, guess) {
            // Credits: algorithm inspired by Apache OpenOffice

            // Calculates the resulting amount
            var irrResult = function(values, dates, rate) {
                var r = rate + 1;
                var result = values[0];
                for (var i = 1; i < values.length; i++) {
                    result += values[i] / Math.pow(r, (dates[i] - dates[0]) / 365);
                }
                return result;
            };

            // Calculates the first derivation
            var irrResultDeriv = function(values, dates, rate) {
                var r = rate + 1;
                var result = 0;
                for (var i = 1; i < values.length; i++) {
                    var frac = (dates[i] - dates[0]) / 365;
                    result -= frac * values[i] / Math.pow(r, frac + 1);
                }
                return result;
            };

            // Initialize dates and check that values contains at least one positive value and one negative value
            var dates = [];
            var positive = false;
            var negative = false;
            for (var i = 0; i < values.length; i++) {
                dates[i] = (i === 0) ? 0 : dates[i - 1] + 365;
                if (values[i] > 0) positive = true;
                if (values[i] < 0) negative = true;
            }

            // Return error if values does not contain at least one positive value and one negative value
            if (!positive || !negative) return '#NUM!';

            // Initialize guess and resultRate
            var guess = (typeof guess === 'undefined') ? 0.1 : guess;
            var resultRate = guess;

            // Set maximum epsilon for end of iteration
            var epsMax = 1e-10;

            // Set maximum number of iterations
            var iterMax = 50;

            // Implement Newton's method
            var newRate, epsRate, resultValue;
            var iteration = 0;
            var contLoop = true;
            do {
                resultValue = irrResult(values, dates, resultRate);
                newRate = resultRate - resultValue / irrResultDeriv(values, dates, resultRate);
                epsRate = Math.abs(newRate - resultRate);
                resultRate = newRate;
                contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax);
            } while (contLoop && (++iteration < iterMax));

            if (contLoop) return '#NUM!';

            // Return internal rate of return
            return resultRate;
        }

        function NPV() {
            // Cast arguments to array
            var args = [];
            for (var i = 0; i < arguments.length; i++) {
                args = args.concat(arguments[i]);
            }
  
            // Lookup rate
            var rate = args[0];
 
            // Initialize net present value
            var value = 0;
  
            // Loop on all values
            for (var j = 1; j < args.length; j++) {
                value += args[j] / Math.pow(1 + rate, j);
            }
 
            // Return net present value
            return value;
        }

        return {
            npv: NPV,
            irr: IRR
        };
    }])

    .factory('roi', ['math', function (math) {

        function chart(volumeInTbV7, annualAvgCostV11, esiRotV23, avgAnnlCostDeclineV19,
            annDataGrowV15, discSpendAvgV27, finesSpendAnnlV31, employeesE59, hourCostAvgE55, infLocSpendV51, hurdleRateK54, taxK52, refreshRetensionV35, jurCountV39, ecmRepoV43, recVendorsV47) {

            function getYear() {
                if(assumptions.c51())
                    return "unknown time";
                
                if (proforma.d30()>0)
                    return "Year 1";
                
                if (proforma.e30()>0)
                    return "Year 2";
                
                if (proforma.f30()>0)
                    return "Year 3";
                
                if(proforma.g30()>0)
                    return "Year 4";
                
                if (proforma.h30()>0)
                    return "Year 5";

                return "Year 6 or later";
            }

            var proforma = {                
              c30: function() {
                  return this.c29();
              },
              c29: function() {
                  return this.c27();
              },
              c27: function() {
                  return this.c4() + this.c5();
              },
              c4: function() {
                  return -(assumptions.c41() + assumptions.c42());
              },
              c5: function () {
                  return this.c4() * assumptions.c34();
              },
              c32: function() {
                  return math.npv(assumptions.c17(), this.d27(), this.e27(), this.f27(), this.g27(), this.h27()) + this.c27();
              },
              c33: function() {
                  return math.irr([this.c27(), this.d27(), this.e27(), this.f27(), this.g27(), this.h27()]);
              },
              //D
              d7: function() {
                  return -(assumptions.c41()*assumptions.c36())-(assumptions.c42()*assumptions.c37());
              },
              d8: function() {
                  return -assumptions.c43();
              },
              d9: function() {
                  return -assumptions.c44();
              },
              d10: function() {
                  return assumptions.d59();
              },
              d11: function() {
                  return assumptions.c33() > 0 ? (this.c4()+this.c5())/assumptions.c33() : 0;
              },
              d12: function() {
                  return assumptions.d66();
              },
              d13: function () {
                  return assumptions.d73();
              },
              d14: function () {
                  return assumptions.d80();
              },
              d15: function() {
                  return this.d7() + this.d8() + this.d9() + this.d10() + this.d11() + this.d12() + this.d13() + this.d14();
              },
              d17: function() {
                  return Math.min(0,-this.d15()*(assumptions.c31()));
              },
              d19: function() {
                  return this.d15() + this.d17();
              },
              d21: function() {
                  return -this.d19() * assumptions.c16();
              },
              d23: function() {
                  return this.d19() + this.d21();
              },
              d25: function() {
                  return -this.d11();
              },
              d27: function() {
                  return this.d23() + this.d25();
              },
              d29: function() {
                  return this.d27() / (1 + assumptions.c17());
              }, 
              d30: function () {
                  return this.c30() + this.d29();
              },
              //E
              e7: function() {
                  return -(assumptions.c41()*assumptions.c36())-(assumptions.c42()*assumptions.c37());;
              },
              e8: function() {
                  return 0;
              },
              e9: function() {
                  return 0;
              },
              e10: function() {
                  return assumptions.e59();
              },
              e11: function() {
                  return assumptions.c33() > 1 ? (this.c4()+this.c5())/assumptions.c33() : 0;
              },
              e12: function() {
                  return assumptions.e66();
              },
              e13: function () {
                  return assumptions.e73();
              },
              e14: function () {
                  return assumptions.e80();
              },
              e15: function() {
                  return this.e7() + this.e8() + this.e9() + this.e10() + this.e11() + this.e12() + this.e13() + this.e14();
              },
              e17: function() {
                  return Math.min(0,-this.e15()*(assumptions.c31()-assumptions.c32()));
              },
              e19: function() {
                  return this.e15() + this.e17();
              },
              e21: function() {
                  return -this.e19() * assumptions.c16();
              },
              e23: function() {
                  return this.e19() + this.e21();
              },
              e25: function() {
                  return -this.e11();
              },
              e27: function() {
                  return this.e23() + this.e25();
              },
              e29: function() {
                  return this.e27() / ((1 + assumptions.c17()) * (1 + assumptions.c17()));
              }, 
              e30: function() {
                  return this.d30() + this.e29();
              },
              e32: function() {
                  return assumptions.c51() ? "Not Applicable" : proforma.c32();
              },
              e33: function () {
                  try {
                      return this.c33();
                  } catch(err) {
                      return "Incomputable";
                  }
              }
              //F
              ,
              f7: function() {
                  return -(assumptions.c41()*assumptions.c36())-(assumptions.c42()*assumptions.c37());
              },
              f8: function() {
                  return 0;
              },
              f9: function() {
                  return 0;
              },
              f10: function() {
                  return assumptions.f59();
              },
              f11: function() {
                  return assumptions.c33() > 2 ? (this.c4()+this.c5())/assumptions.c33() : 0;
              },
              f12: function() {
                  return assumptions.f66();
              },
              f13: function () {
                  return assumptions.f73();
              },
              f14: function () {
                  return assumptions.f80();
              },
              f15: function() {
                  return this.f7() + this.f8() + this.f9() + this.f10() + this.f11() + this.f12() + this.f13() + this.f14();
              },
              f17: function() {
                  return Math.min(0,-this.f15()*(assumptions.c31() - 2*assumptions.c32()));
              },
              f19: function() {
                  return this.f15() + this.f17();
              },
              f21: function() {
                  return -this.f19() * assumptions.c16();
              },
              f23: function() {
                  return this.f19() + this.f21();
              },
              f25: function() {
                  return -this.f11();
              },
              f27: function() {
                  return this.f23() + this.f25();
              },
              f29: function() {
                  return this.f27() / ((1 + assumptions.c17()) * (1 + assumptions.c17()) * (1 + assumptions.c17()));
              }, 
              f30: function() {
                  return this.e30() + this.f29();
              },
              
              //G
              g7: function() {
                  return -(assumptions.c41()*assumptions.c36())-(assumptions.c42()*assumptions.c37());
              },
              g8: function() {
                  return 0;
              },
              g9: function() {
                  return 0;
              },
              g10: function() {
                  return assumptions.g59();
              },
              g11: function() {
                  return assumptions.c33() > 3 ? (this.c4()+this.c5())/assumptions.c33() : 0;
              },
              g12: function() {
                  return assumptions.g66();
              },
              g13: function () {
                  return assumptions.g73();
              },
              g14: function () {
                  return assumptions.g80();
              },
              g15: function() {
                  return this.g7() + this.g8() + this.g9() + this.g10() + this.g11() + this.g12() + this.g13() + this.g14();
              },
              g17: function() {
                  return Math.min(0,-this.g15()*(assumptions.c31() - 3*assumptions.c32()));
              },
              g19: function() {
                  return this.g15() + this.g17();
              },
              g21: function() {
                  return -this.g19() * assumptions.c16();
              },
              g23: function() {
                  return this.g19() + this.g21();
              },
              g25: function() {
                  return -this.g11();
              },
              g27: function() {
                  return this.g23() + this.g25();
              },
              g29: function() {
                  return this.g27() / ((1 + assumptions.c17()) * (1 + assumptions.c17()) * (1 + assumptions.c17()) * (1 + assumptions.c17()));
              }, 
              g30: function() {
                  return this.f30() + this.g29();
              },
              
              //H
              h7: function() {
                  return -(assumptions.c41()*assumptions.c36())-(assumptions.c42()*assumptions.c37());
              },
              h8: function() {
                  return 0;
              },
              h9: function() {
                  return 0;
              },
              h10: function() {
                  return assumptions.h59();
              },
              h11: function() {
                  return assumptions.c33() > 4 ? (this.c4()+this.c5())/assumptions.c33() : 0;
              },
              h12: function() {
                  return assumptions.h66();
              },
              h13: function () {
                  return assumptions.h73();
              },
              h14: function () {
                  return assumptions.h80();
              },
              h15: function() {
                  return this.h7() + this.h8() + this.h9() + this.h10() + this.h11() + this.h12() + this.h13() + this.h14();
              },
              h17: function() {
                  return Math.min(0,-this.h15()*(assumptions.c31() - 4*assumptions.c32()));
              },
              h19: function() {
                  return this.h15() + this.h17();
              },
              h21: function() {
                  return -this.h19() * assumptions.c16();
              },
              h23: function() {
                  return this.h19() + this.h21();
              },
              h25: function() {
                  return -this.h11();
              },
              h27: function() {
                  return this.h23() + this.h25();
              },
              h29: function() {
                  return this.h27() / ((1 + assumptions.c17()) * (1 + assumptions.c17()) * (1 + assumptions.c17()) * (1 + assumptions.c17()) * (1 + assumptions.c17()));
              }, 
              h30: function() {
                  return this.g30() + this.h29();
              }
            };

            var assumptions = {
                e55: function () {
                    return this.d55()*(1+this.c3());
                },
                e57: function () {
                    return this.d57()*(1+this.c25());
                },
                f55: function () {
                    return this.e55()*(1+this.c3());
                },
                f57: function () {
                    return this.e57()*(1+ this.c25());
                },
                g55: function() {
                    return this.f55()*(1 + this.c3());
                },
                g56: function() {
                    return this.g55() * this.c4() * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5());
                },
                g57: function() {
                    return this.f57()*(1+this.c25());
                },
                g58: function() {
                    return this.g57() * this.c4() * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5());
                },
                h55: function () {
                    return this.g55()*(1+this.c3());
                },
                h56: function() {
                    return this.h55() * this.c4() * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5());
                },
                h57: function () {
                    return this.g57()*(1 + this.c25());
                },
                h58: function () {
                    return this.h57() * this.c4() * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5());
                },
                h59: function() {
                    return (this.g56()-this.g58())/2 + (this.h56()-this.h58())/2;
                },
                h66: function() {
                    return this.g65() * this.c19();
                },
                h73: function() {
                    return this.g72()*this.c20();
                },
                h80: function () {
                    return this.g79() * this.c21();
                },
                g59: function() {
                    return (this.f56() - this.f58()) / 2 + (this.g56() - this.g58()) / 2;
                },
                g65: function() {
                    return this.f65() * (1 + this.c26());
                },
                g66: function() {
                    return this.f65() * this.c19();
                },
                g72: function() {
                    return this.f72()*(1+this.c27());
                },
                g73: function() {
                    return this.f72() * this.c20();
                },
                g79: function () {
                    return this.f79() * (1 + this.c28());
                },
                g80: function () {
                    return this.f79() * this.c21();
                },
                f56: function() {
                    return this.f55() * this.c4() * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5());
                },
                f58: function() {
                    return this.f57() * this.c4() * (1 - this.c5()) * (1 - this.c5()) * (1 - this.c5());
                },
                f59: function() {
                    return (this.e56() - this.e58()) / 2 + (this.f56() - this.f58()) / 2;
                },
                f65: function() {
                    return this.e65() * (1 + this.c26());
                },
                f66: function() {
                    return this.e65() * this.c19();
                },
                f72: function() {
                    return this.e72() * (1 + this.c27());
                },
                f73: function() {
                    return this.e72() * this.c20();
                },
                f79: function () {
                    return this.e79() * (1 + this.c28());
                },
                f80: function () {
                    return this.e79() * this.c21();
                },
                e59: function() {
                    return (this.d56() - this.d58()) / 2 + (this.e56() - this.e58()) / 2;
                },
                e65: function() {
                    return this.d65() * (1 + this.c26());
                },
                e66: function() {
                    return this.d65() * this.c19();
                },
                e72: function() {
                    return this.d72() * (1 + this.c27());
                },
                e73: function() {
                    return this.d72() * this.c20();
                },
                e79: function () {
                    return this.d79() * (1 + this.c28());
                },
                e80: function () {
                    return this.d79() * this.c21();
                },
                d65: function() {
                    return this.c63() * (1 + this.c26());
                },
                d66: function() {
                    return this.c62() * this.c19();
                },
                d72: function() {
                    return this.c70() * (1 + this.c27());
                },
                d73: function() {
                    return this.c69() * this.c20();
                },
                d79: function () {
                    return this.c77() * (1 + this.c28());
                },
                d80: function () {
                    return this.c76() * this.c21();
                },
                c28: function () {
                    return this.c25() / 2;
                },
                c27: function() {
                    return this.c25()/2;
                },
                c31: function() {
                    return 20/100;
                },
                c16: function () {
                    return taxK52 / 100;
                },
                c26: function () {
                    return this.c25();
                },
                c43: function() {
                    return this.g105();
                },
                c44: function() {
                    return this.g104();
                },
                c36: function() {
                    return 18 /100;
                },
                c37: function() {
                    return 0;
                },
                g104: function() {
                    return refreshRetensionV35 ? this.e104()*this.f104() : 0;
                },
                g105: function() {
                    return this.e105()* this.f105();
                },
                c17: function() {
                    return hurdleRateK54 / 100;
                },
                c41: function () {
                    return this.e94();
                },
                c42: function () {
                    return this.e101();
                },
                c50: function () {
                    return 18500;
                },
                c9: function () {
                    return jurCountV39;
                },
                c10: function () {
                    return ecmRepoV43;
                },
                c11: function () {
                    return recVendorsV47;
                },
                e94: function () {
                    return 325000;
                    //return this.g88() + this.g89() + this.g90() + this.g91() + this.g92() + this.g93();
                },
                e104: function () {
                    return this.c50();
                },
                e105: function () {
                    return this.e94();
                },
                f104: function () {
                    return this.c9();
                },
                f105: function () {
                    return this.c38();
                },
                e101: function () {
                    return Math.max(this.g96(),this.g97(),this.g98(),this.g99(),this.g100());
                },
                f96: function () {
                    return this.c2();
                },
                e96: function () {
                    return 25000;
                },
                g96: function () {
                    return this.f96() < 10 ? this.e96() : this.e96();
                },
                c97: function () {
                    return 25000;
                },
                d97: function () {
                    return 400;
                },
                e97: function () {
                    return 40000;
                },
                c98: function () {
                    return 40000;
                },
                d98: function () {
                    return 350;
                },
                e98: function () {
                    return 120000;
                },
                e99: function () {
                    return 300000;
                },
                d100: function () {
                    return 250;
                },
                g97: function () {
                    return this.f96() > 10 ? (this.f96() <= 50 ?  Math.min(((this.c97()) + this.d97() * (this.f96() - 10)), this.e97()) : this.e97()) : 0;
                },
                g98: function () {
                    return this.f96() > 50 ? (this.f96 <= 300 ? Math.min((this.c98() + this.d98() * (this.f96() - 50)), this.e98()) : this.e98()) : 0;
                },
                g99: function () {
                    return this.f96() > 300 ? (this.f96() <= 1000 ? Math.min((this.e98() + this.d99() * (this.f96() - 300)), this.e99()) : this.e99()) : 0;
                },
                g100: function () {
                    return this.f96() > 1000 ? (this.e99() + this.d100() * (this.f96() - 1000)) : 0;
                },
                c34: function () {
                    return 0;
                },
                c38: function () {
                    return 40/100;
                },
                c56: function() {
                    return this.c55() * this.c4();
                },
                c55: function() {
                    return this.c2();
                },
                c33: function() {
                    return 3;
                },
                c32: function() {
                    return 1/100;
                },
                c2: function() {
                    return volumeInTbV7;
                },
                c4: function() {
                    return annualAvgCostV11;
                },
                d59: function() {
                    return (this.c56() - this.c58()) + (this.d56() - this.d58()) / 2;
                },
                c58: function() {
                    return this.c57() * this.c4();
                },
                c57: function() {
                    return this.c55() * (1 - this.c15());
                },
                c15: function() {
                    return esiRotV23 / 100;
                },
                c5: function() {
                    return avgAnnlCostDeclineV19 / 100;
                },
                d56: function() {
                    return this.d55() * this.c4() * (1 - this.c5());
                },
                d55: function() {
                    return this.c55() * (1 + this.c3());
                },
                c3: function() {
                    return annDataGrowV15 / 100;
                },
                d58: function() {
                    return this.d57() * this.c4() * (1 - this.c5());
                },
                d57: function() {
                    return this.c57() * (1 + this.c25());
                },
                e56: function() {
                    return this.e55()*this.c4()*(1-this.c5())*(1-this.c5());
                },
                e58: function() {
                    return this.e57() * this.c4() * (1 - this.c5()) * (1 - this.c5());
                },
                c25: function() {
                    return this.c3() / 3;
                },
                c6: function() {
                    return discSpendAvgV27 * 1000;
                },
                c63: function() {
                    return this.c62() * (1 - this.c19());
                },
                c62: function() {
                    return this.c6();
                },
                c19: function() {
                    return this.c3() - this.c25() + 20/100;
                },
                c7: function() {
                    return finesSpendAnnlV31 * 1000;
                },
                c70: function() {
                    return this.c69() * (1 - this.c20());
                },
                c69: function() {
                    return this.c7();
                },
                c20: function() {
                    return 0.8;
                },
                c48: function() {
                    return this.c12() * this.c13() * this.c14() * this.c35() / 60;
                },
                c12: function() {
                    return employeesE59 > 0 ? employeesE59 : 1;
                },
                c13: function() {
                    return hourCostAvgE55 > 0 ? hourCostAvgE55 : 1;
                },
                c14: function() {
                    return infLocSpendV51;
                },
                c35: function() {
                    return 220;
                },
                c77: function() {
                    return this.c76() * (1 - this.c21());
                },
                c21: function() {
                    return 0.33;
                },
                c76: function() {
                    return this.c48();
                },
                c51: function() {
                    return ((this.c2()==1 && this.c3() == 1/100 && this.c4()==1 && this.c6() ==0 && this.c7() == 0 && this.c9() == 1 
                                    && this.c10() == 1 && this.c11() == 0 && this.c12() == 1 && this.c13() == 1 && this.c14() == 0))
                    
                        || ((this.c2() == 1 && this.c3() == 1 / 100 && this.c4() == 1 && this.c6() == 0 && this.c7() == 0 && this.c14() == 0));
                },
                c22: function () {
                    return this.c3();
                },
                d64: function() {
                    return this.c62() * (1 + this.c22());
                },
                c23: function () {
                    return this.c3() / 2 < 0.2 ? 0.2 : this.c3() / 2;
                },
                d71: function () {
                    return this.c69() * (1 + this.c23());
                },
                c24: function () {
                    return this.c3() / 2;
                },
                d78: function () {
                    return this.c76() * (1 + this.c24());
                },
                e64: function() {
                    return this.d64() * (1 + this.c22());
                },
                f64: function() {
                    return this.e64() * (1 + this.c22());
                },
                e71: function () {
                    return this.d71() * (1 + this.c23());
                },
                f71: function () {
                    return this.e71() * (1 + this.c23());
                },
                e78: function () {
                    return this.d78() * (1 + this.c24());
                },
                f78: function () {
                    return this.e78() * (1 + this.c24());
                },
                g64: function() {
                    return this.f64() * (1 + this.c22());
                },
                h64: function() {
                    return this.g64() * (1 + this.c22());
                },
                g71: function () {
                    return this.f71() * (1 + this.c23());
                },
                h71: function () {
                    return this.g71() * (1 + this.c23());
                },
                g78: function () {
                    return this.f78() * (1 + this.c24());
                },
                h78: function () {
                    return this.g78() * (1 + this.c24());
                },
                h65: function() {
                    return this.g65() * (1 + this.c26());
                },
                h72: function () {
                    return this.g72() * (1 + this.c27());
                },
                h79: function () {
                    return this.g79() * (1 + this.c28());
                },

            };

            //column chart
            var d3 = assumptions.c56();
            var d17 = assumptions.c56() - assumptions.d59();
            var d4 = assumptions.c6();
            var d18 = assumptions.c63();
            var d5 = assumptions.c7();
            var d19 = assumptions.c70();
            var d6 = assumptions.c48();
            var d20 = assumptions.c77();
            var d28 = getYear();
            var d10 = -proforma.c4();
            var d11 = -proforma.d7();
            var d12 = 0;
            var d13 = assumptions.g105();
            var d14 = d10 + d11 + d12 + d13;
            var d35 = d14; // 1000;

            //line chart
            var g12 = d3;
            var g13 = d4;
            var g14 = d5;
            var g15 = d6;
            var g16 = g12 + g13 + g14 + g15;

            var h12 = assumptions.d56();
            var h13 = assumptions.d64();
            var h14 = assumptions.d71();
            var h15 = assumptions.d78();
            var h16 = h12 + h13 + h14 + h15;

            var i12 = assumptions.f56();
            var i13 = assumptions.f64();
            var i14 = assumptions.f71();
            var i15 = assumptions.f78();
            var i16 = i12 + i13 + i14 + i15;

            var j12 = assumptions.h56();
            var j13 = assumptions.h64();
            var j14 = assumptions.h71();
            var j15 = assumptions.h78();
            var j16 = j12 + j13 + j14 + j15;

            var l12 = assumptions.d58();
            var l13 = assumptions.d65();
            var l14 = assumptions.d72();
            var l15 = assumptions.d79();
            var l16 = l12 + l13 + l14 + l15;

            var m12 = assumptions.f58();
            var m13 = assumptions.f65();
            var m14 = assumptions.f72();
            var m15 = assumptions.f79();
            var m16 = m12 + m13 + m14 + m15;

            var n12 = assumptions.h58();
            var n13 = assumptions.h65();
            var n14 = assumptions.h72();
            var n15 = assumptions.h79();
            var n16 = n12 + n13 + n14 + n15;


            
            var chartData = {
                
                roi: {
                    Storage: [d3, d17],
                    Discovery: [d4, d18],
                    Risk: [d5, d19],
                    Prod: [d6, d20],
                    Current: [g16, h16, i16, j16],
                    IGS: [g16, l16, m16, n16]
                },
                values: {
                    NPV: proforma.e32(),
                    IRR: proforma.e33(),//'Not implemented',
                    Year: d28,
                    ROI: d35
                }
            };

            return chartData;
        }

        return chart;
    }]);