﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers', ['highcharts-ng', 'ui.slider', 'app.services'])

    // Path: /
    .controller('HomeCtrl', ['$scope', '$location', '$window', 'roi', function ($scope, $location, $window, roi) {

        var series = [
            {
                name: 'Productivity',
                data: [],
                color: '#8085e9',
                id: 'series-1'
            },
            {
                name: 'Risk/Compliance',
                data: [],
                color: '#90ed7d',
                id: 'series-2'
            },
            {
                name: 'E-Discovery',
                data: [],
                color: '#f7a35c',
                id: 'series-3'
            },
            {
                name: 'Storage',
                data: [],
                color: '#7cb5ec',
                id: 'series-4'
            }
        ];

        var lineSeries = [
            {
                name: 'Current Growth Rate',
                data: [],
                color: '#5b9bd5',
                id: 'series-1'
            }, {
                name: 'IGS Growth Rate',
                data: [],
                color: '#ff4500',
                id: 'series-2'
            }
        ];

        function getSeries(data) {
            series[0].data = data.Prod;
            series[1].data = data.Risk;
            series[2].data = data.Discovery;
            series[3].data = data.Storage;

            return series;
        }

        function getLineSeries(data) {
            lineSeries[0].data = data.Current;
            lineSeries[1].data = data.IGS;
            return lineSeries;
        };

        function reload() {
            var loaded;
            return function() {
                if (loaded) {
                    var data = chartData();
                    $scope.chartConfig.series = getSeries(data.roi);
                    $scope.lineChartConfig.series = getLineSeries(data.roi);
                    loadValues(data.values);
                }
                loaded = true;
            };
        }
        
        function chartData() {
            return new roi($scope.volume, $scope.annlAvgCost, $scope.esiRot,
                $scope.avgAnnlCostDec, $scope.annDataGrow, $scope.discSpendAvg, $scope.finesSpendAnnl,
                $scope.employees, $scope.hourCostAvg, $scope.infLocSpend, $scope.hurdleRate, $scope.tax, $scope.refreshRetension == 'true', $scope.jurCount, $scope.ecmRepo, $scope.recVendors
            );
        };
        
        function loadValues(values) {
            $scope.npv = values.NPV;
            $scope.irr = values.IRR;
            $scope.year = values.Year;
            $scope.roi = values.ROI;
        }

        $scope.volume = 1500;
        $scope.annlAvgCost = 15000;
        $scope.esiRot = 50;
        $scope.avgAnnlCostDec = 15;
        $scope.annDataGrow = 50;
        $scope.discSpendAvg = 3500;
        $scope.finesSpendAnnl = 5000;
        $scope.employees = 200;
        $scope.hourCostAvg = 40;
        $scope.infLocSpend = 67;
        
        //to add editors
        $scope.hurdleRate = 10;
        $scope.tax = 39;
        $scope.refreshRetension = 'true';
        $scope.jurCount = 3;
        $scope.ecmRepo = 1;
        $scope.recVendors = 2;

        var chart = chartData();

        loadValues(chart.values);

        $scope.chartConfig = {
            options: {
                chart: {
                    type: 'column',
                    margin: 75,
                    height: 300,
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 100,
                        viewDistance: 25
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        depth: 200
                    }
                }
            },
            series: getSeries(chart.roi),
            title: {
                text: 'Recall Crystal: Return on Investment' 
            },
            loading: false,
            xAxis: {
                categories: [
                    'Now',
                    'Post Implementation End of Year 1'
                    ]
            }
        };

        $scope.lineChartConfig = {
            options: {
                chart: {
                    type: 'line',
                    height: 290
                }
            },
            title: {
                text: 'Cost of Delaying IG Implementation',
                x: -20 //center
            },
            xAxis: {
                categories: ['Now', 'End of Year 1', 'End of Year 3', 'End of Year 5']
            },
            yAxis: {
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: 'M'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: getLineSeries(chart.roi)
        };

        $scope.$watch('volume', reload());
        $scope.$watch('annlAvgCost', reload());
        $scope.$watch('esiRot', reload());
        $scope.$watch('avgAnnlCostDec', reload());
        $scope.$watch('annDataGrow', reload());
        $scope.$watch('discSpendAvg', reload());
        $scope.$watch('finesSpendAnnl', reload());
        $scope.$watch('employees', reload());
        $scope.$watch('hourCostAvg', reload());
        $scope.$watch('infLocSpend', reload());
        
        $scope.$watch('hurdleRate', reload());
        $scope.$watch('tax', reload());
        $scope.$watch('refreshRetension', reload());
        $scope.$watch('jurCount', reload());

        $scope.$root.title = 'AngularJS SPA Template for Visual Studio';
//        
//        $scope.$on('$viewContentLoaded', function () {
//            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        //        });
        
        //validation and help
        $scope.isAnnlAvgCostValid = function () {
            return $scope.annlAvgCost >= 4000 && $scope.annlAvgCost <= 15000;
        };
        
        $scope.isAnnDataGrowValid = function () {
            return $scope.annDataGrow >= 30 && $scope.annDataGrow <= 50;
        };
        
        $scope.isAvgAnnlCostDecValid = function () {
            return $scope.avgAnnlCostDec <= 20;
        };
        
        $scope.isEsiRotValid = function () {
            return $scope.esiRot >= 40 && $scope.esiRot <= 80;
        };

        $scope.isDiscSpendAvgValid = function () {
            return $scope.discSpendAvg >= 100 && $scope.discSpendAvg <= 10000;
        };

        $scope.isFinesSpendAnnlValid = function () {
            return $scope.finesSpendAnnl >= 100 && $scope.finesSpendAnnl <= 3000;
        };

        $scope.isJurCountValid = function () {
            return $scope.jurCount >= 1 && $scope.jurCount <= 5;
        };

        $scope.isInfLocSpendValid = function () {
            return $scope.infLocSpend >= 48 && $scope.infLocSpend <= 72;
        };
        

        $scope.isHourCostAvgValid = function () {
            return $scope.hourCostAvg >= 25 && $scope.hourCostAvg <= 80;
        };
        
        $scope.getAnnlAvgCostPopover = function () {
            return 'The Industry range for IT cost per TB is $4,000 - $15,000. Choose an appropriate cost. Source: IDC and IBM';
        };
    }])

    // Path: /about
    .controller('AboutCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA | About';
//        $scope.$on('$viewContentLoaded', function () {
//            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
//        });
    }])

    // Path: /login
    .controller('LoginCtrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'AngularJS SPA | Sign In';
        // TODO: Authorize a user
        $scope.login = function () {
            $location.path('/');
            return false;
        };
//        $scope.$on('$viewContentLoaded', function () {
//            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
//        });
    }])

    // Path: /error/404
    .controller('Error404Ctrl', ['$scope', '$location', '$window', function ($scope, $location, $window) {
        $scope.$root.title = 'Error 404: Page Not Found';
//        $scope.$on('$viewContentLoaded', function () {
//            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
//        });
    }]);